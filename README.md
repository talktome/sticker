# Sticker - synchronized ticker

## Overview

Prints current time in some format with some frequency, provided the output differs.

```
Usage:  sticker [OPTIONS]

Options:
  -f, --format string   Rewrite the default time in a format you choose
                         (default "Mon Jan 2 15:04:05 -0700 MST 2006")
  -v, --frequency int   Read computer clock with given frequency/Hz, 1-1000
                         (default 1)
  -h, --help            Print usage
```

## Installation

```
go get -u bitbucket.org/talktome/sticker
```
