package main

import (
	"time"
)

const (
	maxFrequency = 1000
)

type SyncedTicker struct {
	C     <-chan time.Time
	stopC chan interface{}
}

func NewSyncedTicker(frequency int) *SyncedTicker {
	var interval int = 1e9
	if frequency < -1 {
		interval = -1e9 * frequency
	} else if frequency > 1 {
		interval = 1e9 / frequency
	} else if frequency > maxFrequency {
		interval = 1e9 / maxFrequency
	}

	c := make(chan time.Time)
	t := &SyncedTicker{c, make(chan interface{})}
	go t.start(c, interval)
	return t
}

func (t *SyncedTicker) start(c chan time.Time, interval int) {
	for {
		select {
		case <-t.stopC:
			return
		default:
			if time.Duration(interval-(time.Now().Nanosecond()%interval)) > 100*time.Microsecond {
				time.Sleep(time.Duration(interval - (time.Now().Nanosecond() % interval)))
			} else {
				time.Sleep(time.Duration(2*interval - (time.Now().Nanosecond() % interval)))
			}
			c <- time.Now()
		}
	}
}

func (t *SyncedTicker) Stop() {
	t.stopC <- nil
}
