package main

import (
	"fmt"
	"os"

	"github.com/spf13/pflag"
)

func main() {
	var (
		form string
		freq int
		help bool
	)
	pflag.StringVarP(&form, "format", "f", "Mon Jan 2 15:04:05 -0700 MST 2006", "To define format, rewrite the default time in\na format you choose.\n")
	pflag.IntVarP(&freq, "frequency", "v", 1, "Read system clock with given frequency/Hz, 1-1000.\nNegative values give period/s.\n")
	pflag.BoolVarP(&help, "help", "h", false, "Print usage.")
	pflag.Parse()

	if help {
		fmt.Println("Sticker - synchronized ticker\n")
		fmt.Println("Prints current time in some format with some frequency, provided the\noutput differs.\n")
		fmt.Println("Usage: ", os.Args[0], " [OPTIONS]\n")
		fmt.Println("Options:")
		pflag.PrintDefaults()
		os.Exit(0)
	}

	ticker := NewSyncedTicker(freq)
	defer ticker.Stop()
	oldStr := ""
	for {
		newStr := (<-ticker.C).Format(form)
		if oldStr != newStr {
			fmt.Println(newStr)
			oldStr = newStr
		}
	}
}
